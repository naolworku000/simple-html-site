document.getElementById('search-input').addEventListener('keyup', async (e) => {
    // Search comments
    // Use this API: https://jsonplaceholder.typicode.com/comments?postId=3
    // Display the results in the UI

    // Things to look out for
    // ---
    // Use es6
    let val = document.getElementById('search-input').value;

    /*
    It's better to use the below two lines outside this event listener to minimize cost 
    and increase performance since we are filtering the characters here not in the backend.
    Just to do it with the flow instruction i left it here
    */
    const res = await fetch('http://localhost:8000');
    const json = await res.json() ?? [];
    
    let name = json.map(e => (e.name));

    name = name.filter(n =>  n.includes(val));

    
    const result = name.length ? `<li>${name.join('</li><li>')}</li>` : `<li>No matching comments!</li>`;
    document.getElementById('results').innerHTML = result

})